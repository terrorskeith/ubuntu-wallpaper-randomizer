Simple Background Randomizer for Ubuntu. Allows customizable randomization for personal use.


Added this line to 'crontab -e'

`* * * * * bash /home/user/path/to/randomize_wallpaper.sh`

This will randomize and set the wallpaper every minute based on directory set in the script.

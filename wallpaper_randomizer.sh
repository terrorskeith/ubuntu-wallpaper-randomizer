#!/bin/bash

# Found this part in https://askubuntu.com/questions/140305/cron-not-able-to-succesfully-change-background

# Find the user
user=$(whoami)

# Find the PIDs for the gnome session, then loop through the PIDs to find the newest PID for later use
fl=$(find /proc -maxdepth 2 -user $user -name environ -print -quit)
while [ -z $(grep -z DBUS_SESSION_BUS_ADDRESS "$fl" | cut -d= -f2- | tr -d '\000' ) ]
do
  fl=$(find /proc -maxdepth 2 -user $user -name environ -newer "$fl" -print -quit)
done

# Set the required environment variable
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS $fl | cut -d= -f2-)

# The above portion was found to address an issue where gsettings required the DBUS_SESSION_BUS_ADDRESS variable to be set to function

#Grab all the wallpapers in the directory
wallpaper_images_array=(/home/user/path/to/wallpapers/*)

#Pick a random number from the total number of wallpapers
random_num=$(($RANDOM % ${#wallpaper_images_array[@]}))

#Set the background based on a random element in the wallpaper images array
gsettings set org.gnome.desktop.background picture-uri file://${wallpaper_images_array[$random_num]}
